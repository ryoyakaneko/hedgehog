import numpy as np
from sklearn.metrics import auc

def detect(size): # size:各時刻での窓のサイズを並べた列
    n = len(size)
    change = [0] ###
    for i in range(1,n):
        if not size[i] - size[i - 1] == 1:
            change.append(i) # 検知した変化点を格納
    change.append(0)
    change.append(0)
    return change


def InvRunLen(size): # Inverse run lengthを計算
    n = len(size)
    change = detect(size)
    inv_run_len = np.zeros(n)
    for i in range(0, n):
        if round(size[i] - (1.0 + i)) == 0.0:
            inv_run_len[i] = 0
        else:
            inv_run_len[i] = 1/(size[i] + 1)
    return inv_run_len


def alarm(score, beta): # beta以上でalarmを上げる
    n = len(score)
    indicator = np.zeros(n)
    for i in range(0, n):
        if score[i] > beta:
            indicator[i] = 1 
    return indicator


def benefit(score, t_true, T): 
    n = len(score)
    b = np.zeros(n)
    m = len(t_true)
    for i in range(0, m):
#        for j in range(t_true[i] - T + 1, t_true[i] + T): # フライング+遅れ    
#        for j in range(int(t_true[i]), n):
        for j in range(int(t_true[i]), min(int(t_true[i]) + T, n)): # 遅れのみ
            b[j] = 1 - np.absolute(j - t_true[i]) / T  
#            b[j] = 1 # 遅れに寛容
#            b[j] = 1/(j-t_true[i] + 1)
#            b[j] = np.exp(-(j - t_true[i])) #遅れに厳しい
    return b



def B(score, beta, t_true, T):
    a = alarm(score, beta)
    b = benefit(score, t_true, T)
    return a.dot(b)


def supB(score, t_true, T):
    return sum(benefit(score, t_true, T))


def N(score, beta, t_true, T):
    n = len(score)
    a = alarm(score, beta)
    b = benefit(score, t_true, T)
    s = 0
    for i in range(0, n):
        if b[i] == 0:
            s = s + a[i]
    return s


def supN(score, t_true, T):
    n = len(score)
    s = 0
    b = benefit(score, t_true, T)
    for i in range(0, n):
        if b[i] == 0:
            s = s + 1
    return s

def calc_auc(score, t_true, T):
    x = []
    y = []
    p = [min(score)-1] + sorted(score)
    for beta in p:
        e = B(score, beta, t_true, T)
        f  = N(score, beta, t_true, T)
        g = supB(score, t_true, T)
        h = supN(score, t_true, T)
        x.append(f/h)
        y.append(e/g)
    return auc(x,y)
            
###############################################################

def detect2(size):
    n = len(size)
    change = []
    for i in range(1, n):
        if not size[i] - size[i - 1] == 1:
            change.append(i - size[i]) # 検知した変化点を格納
    return change 

def detect3(score, beta):
    raise_detect = np.where(score > beta)[0] # scoreがbetaより高い時刻を格納
    n = len(raise_detect)
    s = 0 # s: 区間の左端
    t = 0 
    change = [] 
    while s <= n - 1:
        t = s + 1 # t: 区間の右端　 + 1
        while t <= n - 1 and raise_detect[t] - raise_detect[t - 1] == 1:
            t += 1
        change.append(raise_detect[s] + np.argmax(score[raise_detect[s] :raise_detect[t-1] + 1])) # 区間内でscoreのmaxをとる時刻を格納
        s = t
    return change
    

def TruePositive(change, t_true, T = 20):
    m = len(t_true)
    true_positive = 0
    if len(change) == 0:
        return 0
    idx_list = [] # 重複を避けるため、一度取得されたchangeのindexを格納しておく
    for i in range(0, m):
        idx = np.argmin([abs(t_true[i] - x) for x in change]) # t_true[i]に最も近いchangeのindexを取得
        if abs(change[idx] - t_true[i]) <= T:
            if not idx in idx_list:
                idx_list.append(idx)
                true_positive += 1
    return true_positive 


def precision_recall(change, t_true, T = 20):
    n = len(change)
    m = len(t_true)
    TP = TruePositive(change, t_true, T)
    FP = n - TP
    FN = m - TP
    if n == 0:
        precision = np.nan
    else:
        precision = TP/(TP + FP)
    recall = TP / (TP + FN)
    return np.array([recall, precision])
               
               
               
def benefit2(score, change, t_true, T):
    n = len(score)
    b = np.zeros(n)
    m = len(t_true)
    ben = 0
    for i in range(0, m):
        idx = np.argmin([abs(t_true[i] - x) for x in change]) # t_true[i]に最も近いchangepointのindexを取得
        if abs(change[idx] - t_true[i]) <= T:
            ben += 1 - abs(change[idx] - t_true[i]) / T
    return ben               
               
               
    
