### adwin.py
import numpy as np
from collections import deque

class ADWIN:

    def __init__(self,a=None):
        self.a = 1 if a is None else a

    
    def transform(self, X, delta):
        window = deque([])
        n = len(X)
        size = np.zeros(n)
        window.append(X[0])
        size[0] = 1
        for t in range(1,n):
            window.append(X[t])
            list_window = list(window)
            var_data = np.var(list_window)
            score = adwin(list_window)
            while self.shrink(score, var_data, delta) == True and len(window) > 1: 
                window.popleft() # drop only one element 
                list_window = list(window)
                var_data = np.var(list_window)
                score = adwin(list_window)
            size[t] = len(window)
        return size

    def shrink(self, score, var_data, delta):
        n = len(score)
        istrue = np.zeros(n)
        for i in range(0,n):
            n0 = i + 1
            n1 = n + 1  - n0 ### since number of the data is n+1
            m = 1/(1/n0 + 1/n1)
            _delta = delta / np.log(n + 1)
#            epsilon = np.sqrt(((max - min) * r) ** 2/(2 * m) * np.log(4/_delta)) # conservative threshold (with theoritical assurance)
            epsilon = np.sqrt(2/m * var_data * np.log(2/_delta)) + 2/(3 * m) * np.log(2/_delta) # non-conservative threshold  (using gaussian approximation)
            if score[i] > epsilon: 
                istrue[i] = True # if there exists a score that exceeds epsilon, decide to drop one element
        return any(istrue)


def adwin(X):

    # calculate a list consisting of  differences of sample means that are calculated at  each split

    n = len(X)
    Xmat = X.reshape((n, -1))
    n, m = X.shape
    sum_x = np.zeros((n,m)) # cummulated summation of sample vectors
    sum_x[0] = Xmat[0]
    for i in range(0, n-1):
        sum_x[i+1] = sum_x[i] + Xmat[i+1]
    score = np.zeros(n-1)  
    for cut in range(1,n):
        mu0 = sum_x[cut - 1] / cut
        mu1 = (sum_x[n-1] - sum_x[cut-1]) / (n - cut)
        score[cut - 1] = np.absolute(mu0 - mu1)
    return score



    # def transform2(self, X, delta, r):
    #     window = deque([])
    #     n = len(X)
    #     size = np.zeros((2, n))
    #     window.append(X[0])
    #     size[0, 0] = 1
    #     size[1, 0] = 0
    #     for t in range(1,n):
    #         score_max = -10
    #         window.append(X[t])
    #         list_window = list(window)
    #         var = np.var(list_window)
    #         score = adwin(list_window)
    #         if len(score) > 0 and score_max < max(score):
    #             score_max = max(score)
    #         while self.shrink(score, var, delta, r) == True and len(window) > 1:
    #             window.popleft()
    #             list_window = list(window)
    #             var = np.var(list_window)
    #             score = adwin(list_window)
    #             if len(score) > 0 and score_max < max(score):
    #                 score_max = max(score)
    #         size[0, t] = len(window)
    #         size[1, t] = score_max
    #     return size
