# -*- coding: utf-8 -*-

import numpy as np
from scipy.linalg import qr, solve
from scipy.misc import logsumexp
from scipy.special import gammaln

EPS = np.finfo(np.float).eps


def cpscoring2(data, k1, T1, k2, T2, r):
    '''
    % ***機能***
    % 与えられた一次元データ配列に対する変化点スコアを二段階学習および平滑化
    % によって計算する．スコアリングにはSDNML符号長を使う．誤差分散の推
    % 定量の更新式に忘却を入れるパターン．
    #
    % ***入力***
    % data:データ配列(一次元ベクトル)
    % k1:第一段階学習におけるARモデルの次数．
    % T1:第一段階スコアリングにおける平滑化幅．
    % k2:第二段階学習におけるARモデルの次数．
    % T2:第二段階スコアリングにおける平滑化幅．
    % r:忘却係数

    % ***出力***
    % cp:変化点スコアの列．

    % 質問等あればtosh.914@gmail.comにどうぞ．
    '''

    sdnml1 = calcsdnmlcl2(data, k1, r)
    nsdnml1 = normalize(sdnml1, T1)

    sdnml2 = calcsdnmlcl2(nsdnml1, k2, r)
    cp = normalize(sdnml2, T2)
    return cp


def calcsdnmlcl2(data, k, r):
    '''
    % ***機能***
    % 忘却型逐次的最尤推定法(SDNML)に従って，時系列に対して符号長を与える．
    % 誤差分散の推定量tauに忘却を入れるバージョン(詳細は卒論参照)．

    % ***入力***
    % data:データ配列(一次元ベクトル)
    % k:ARモデルの次数
    % r:忘却係数

    % ***出力***
    % cls:SDNML符号長の列

    % 質問等あればtosh.914@gmail.comにどうぞ．
    '''

    length = data.shape[0]

    eh = np.zeros(length)
    th = np.zeros(length)
    cls = np.zeros(length)

    kk = k + find_none(data)
    m = kk
    eh[:kk] = None
    eh[:kk] = None
    cls[:kk] = None

    # %%% t=k+1のパラメータを得る %%%
    t = kk
    x = data[t]
    if t - k - 1 == -1:
        xb = data[(t - 1)::-1].reshape((k, 1))
    else:
        xb = data[(t - 1):(t - k - 1):-1].reshape((k, 1))
    # invVt=pinv(r*xb*xb')

    # invVt = np.eye(k)
    eps = 1e-15
    Vt = np.eye(k)
    Vt = Vt * np.asscalar(xb.T @ xb)
    eps_I = np.eye(k) * eps
    # 1.0e3 * EPS)

    # M=r*xb*x
    M = xb * x
    At = solve(Vt + eps_I, M)
    eh[t] = x - At.T @ xb
    th[t] = eh[t] ** 2
    cls[t] = None

    # %%% t=k+2からループ開始 %%%
    for t in range(kk + 1, length):
        x = data[t]
        xb = data[(t - 1):(t - k - 1):-1].reshape((k, 1))
        M = (1 - r) * M + r * x * xb
        ct = r * np.asscalar(xb.T @ solve(Vt + eps_I, xb))
        temp = solve(Vt + eps_I, xb)
        Vt = r * xb @ xb.T + (1 - r) * Vt
        At = solve(Vt + eps_I, M)
        dt = ct / (1 - r + ct)
        eh[t] = x - np.asscalar(At.T @ xb)
        th[t] = (1 - r) * th[t - 1] + r * (eh[t] ** 2)

        a = t - m + 1
        b = t - m

        lnK = np.log(np.pi) / 2 + np.log(1 - r) / 2 - np.log(r) / 2 - a * np.log(1 - r) / 2 - np.log(1 - dt) +\
            gammaln(b / 2) - gammaln(a / 2)
        sdnmlcl = lnK + a * np.log(th[t] + eps) / 2 - b * np.log(th[t - 1] + eps) / 2
        if not np.isinf(sdnmlcl):
            cls[t] = sdnmlcl
        else:
            cls[t] = None
    return cls


def normalize(data, T):
    """
    % ***機能***
    % 与えられた一次元データ配列を，幅Tの窓を用いて平滑化する．

    % ***入力***
    % data:データ配列(一次元ベクトル)
    % T:平滑化幅

    % ***出力***
    % x:平滑化されたデータ列．入力と長さは同じ．

    % 質問等あればtosh.914@gmail.comにどうぞ．
    """
    x = np.zeros(data.shape)
    for i in range(1, data.shape[0] + 1):
        x[i - 1] = nanmean(data[max(0, i - T):i])
    return x


def find_none(arr):
    for i in range(len(arr)):
        if not np.isnan(arr[i]):
            return i
    raise ValueError


def nanmean(arr):
    no_nan_arr = [a for a in arr if not np.isnan(a)]
    return np.mean(no_nan_arr) if len(no_nan_arr) > 0 else None


def mldivide(A, b):
    x1, res, rnk, s = np.linalg.lstsq(A, b)
    if rnk == A.shape[1]:
        return x1  # nothing more to do if A is full-rank
    Q, R, P = qr(A.T, mode='full', pivoting=True)
    Z = Q[:, rnk:].conj()
    C = np.linalg.solve(Z[rnk:], -x1[rnk:])
    return x1 + Z.dot(C)


def CFlog(data, k, r):
    """
    :param data:dataは変化点検出をしたい元時系列データ（縦ベクトル）
    :param k:kは変化点検出アルゴリズムのＡＲモデルの次数
    :param r:rは忘却係数（論文参照
    :return:結果の密度関数を与える
    """
    if len(data.shape) == 1:
        data = data.reshape((data.shape[0], 1))

    N = data.shape[0]
    data = np.vstack((np.zeros(data.shape[1]), data))
    result = np.zeros(N + 1)

    # k, k + 1, k + 2, ..., 2k - 1のデータで最初の推定を行う
    # 最初の推定は最小二乗法を用いた
    X = np.zeros((k, k))
    for i in range(k):
        X[i] = data[(k + 1 - i - 1):(2 * k + 1 - i - 1), 0]
    # yn = data[(k+1):(2*k+1)]
    # # XX'b=_X*yn
    # w = mldivide(_X @ _X.T, _X @ yn)

    n = 2 * k
    # 初期パラメータを計算する
    mu = np.mean(data[(k + 1):(n + 1)])
    czero = 0
    var_init = np.sum((data[(k + 1): (n + 1)] - mu) ** 2)
    for j in range(1, k + 1):
        czero += var_init
        czero /= (n - k)

    C = np.zeros(k)
    for j in range(k):
        C[j] = np.mean((data[(k + 1):(n + 1)] - mu) * (data[(k + 1 - j - 1):(n + 1 - j - 1)] - mu))

    # Yule - Walker方程式をつくる
    A = np.zeros((k, k))
    for j in range(k):
        for i in range(k):
            if i == j:
                A[i, i] = czero
            else:
                A[i, j] = C[abs(i - j) - 1]
    # w = mldivide(A, C)
    # w = np.linalg.lstsq(A, C)[0]
    w = np.linalg.solve(A, C)
    bt1 = w.reshape((k, 1))
    xt = data[(2 * k - 1):(k - 1):-1]
    Vt = np.linalg.inv(X @ X.T)
    Xt1 = X @ X.T - xt @ xt.T
    # Vt1 = np.linalg.inv(Xt1 @ Xt1.T)
    ft1 = mldivide(Xt1 @ Xt1.T, xt)
    # ct = xt.T @ Vt1 @ xt
    # bt = bt1 + (Vt1 @ xt) * (data[2 * k] - xt.T @ bt1) / (1 + ct)
    ct = xt.T @ ft1
    bt = bt1 + (ft1) * (data[2 * k] - xt.T @ bt1) / (1 + ct)
    dt = xt.T @ Vt @ xt

    n = N - 2 * k
    # 次に誤差を格納するベクトルをehatにきめる
    log_ehat = np.zeros(n + 1)
    log_ehat[0] = np.log(np.abs((1.0 - dt)[0][0])) + np.log(np.abs(data[2 * k] - bt1.T @ xt))
    log_tau = 2 * log_ehat[0]
    # 一回目の計算終了以下ループに入る, 忘却パラメータとしてrを追加

    # t = 2k + 1 からループ開始
    # test04では、忘却パラメータrを誤差ehatに適用する。

    # tauは各時刻における予測誤差分散の平均
    G = np.sqrt(np.pi)
    for i in range(1, n + 1):
        xt = (data[(2 * k - 1 + i):(k + i - 1):-1]).reshape((k, 1))
        Vt1 = Vt
        ct = xt.T @ Vt1 @ xt
        Vt = Vt1 - Vt1 @ xt @ xt.T @ Vt1 / (1 + ct)
        bt1 = bt
        bt = bt1 + (Vt1 @ xt) @ (data[2 * k + i] - xt.T @ bt1) / (1 + ct)
        dt = xt.T @ Vt @ xt

        # ここまででパラメータ推定は終了
        # 次に予測確率密度関数を求める
        log_ehat[i] = np.log(np.abs((1.0 - dt)[0][0])) + np.log(np.abs(data[2 * k + i] - np.sum(bt1 * xt)))
        # 新しいKの値を求める
        K = np.sqrt((1 - r) / (r * i)) * np.sqrt(np.pi) * G / (1 - dt)
        # 次のGの値を求めておく
        G = 2 / (i * G)

        log_tau1 = log_tau
        first = np.log(1 - r) + np.log(i) + log_tau1 - np.log(i + 1)
        second = np.log(r) + log_ehat[i] * 2 - np.log(i + 1)
        log_tau = logsumexp((first, second))

        log_temp2 = log_tau1 - (np.log(1 + (1 / i)) + log_tau)

        logfhat = -np.log(K) - 0.5 * (np.log(i) + log_tau1) + (i + 1) * log_temp2 / 2

        result[2 * k + i] = logfhat
    # 得られたresultに格納される縦ベクトル（大きさは元の時系列と同じ
    return result[1:]


def CFNormalizer(data, k1, k2, T, T2, r=0.001):
    length = data.shape[0]

    # 平均スコア系列
    score = -100 * data
    z = np.zeros(length)

    for i in range(k1, length):
        z[i] = np.mean(score[(i - T + 1):(i + 1)])

    res = CFlog(z[(2 * k1):], k2, r)
    sofres = res.shape[0]

    res = np.hstack((np.zeros(2 * k1), res))

    datamean = np.mean(data)
    Sc = -datamean * res

    y = np.zeros(length)
    for i in range(T2, length + 1):
        y[i - 1] = np.mean(Sc[(i - T2):i])
    return y


def CFNormalizer2(data, k1, k2, T, T2, r=0.001):
    re = CFlog(data, k1, r)
    length = re.shape[0]

    # 平均スコア系列
    score = -100 * re
    z = np.zeros(length)

    for i in range(k1, length):
        z[i] = np.mean(score[(i - T + 1):(i + 1)])
    # z_mean = z[k1:]
    # for i in range(2*k1):

    res = CFlog(z[(2 * k1):], k2, r)
    sofres = res.shape[0]

    res = np.hstack((np.zeros(2 * k1), res))

    datamean = np.mean(data)
    Sc = -datamean * res

    y = np.zeros(length)
    for i in range(T2, length + 1):
        y[i - 1] = np.mean(Sc[(i - T2):i])
    return y


def alarm1(scores, N, rho, lam, r):
    """
    % ***機能***
    % 動的しきい値選択(詳細は高橋の卒論参照)によりスコアデータに
    % 対してアラームを上げる関数．

    % ***入力***
    % scores:スコアデータ列(横ベクトル)
    % N:セルの数
    % rho:閾値パラメータ
    % lam:推定パラメータ
    % r:忘却係数

    % ***出力***
    % alm:アラームのオン・オフを表すベクトル．入力データ列と長さは同じ．

    % 質問等あればtosh.914@gmail.comにどうぞ．
    """
    #  datasize
    M = scores.shape[0]
    min = np.nanmin(scores)
    # %min = np.nanmean(scores)-3*np.nanstd(scores)
    # %max = np.nanmax(scores);
    max = np.nanmean(scores) + 3 * np.nanstd(scores, ddof=1)

    # alarm on or off
    alm = np.zeros(M)
    # uniform    distribution
    q1 = np.zeros(N) + 1.0 / N
    # % cell width
    w = (max - min) / (N - 2)
    # % left border of the cells
    b = np.zeros(N - 1)
    # calculate left border of the cells
    for i in range(N - 2):
        b[i] = min + w * i
    b[N - 2] = max

    for i in range(M):
        q = (q1 + lam) / (np.sum(q1) + N * lam)
        # find and calc threshold
        l = 0
        for j in range(N):
            if np.sum(q[:(j + 1)]) < 1.0 - rho:
                l = j + 1
        eta = b[l - 1]
        # % decide alarm or not
        if scores[i] >= eta:
            alm[i] = 1

        # find the cell which the score drop in
        h = 0
        for j in range(N - 1):
            if scores[i] >= b[j]:
                h = j + 1

        # % recalc probability of the cells
        q1 = (1.0 - r) * q1
        q1[h] += r

    return alm
