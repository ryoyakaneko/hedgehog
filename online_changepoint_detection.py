from __future__ import division
import numpy as np
from scipy import stats
from abc import ABCMeta, abstractmethod


def online_changepoint_detection(data, hazard_func, observation_likelihood):
    """
    Compute posterior probability of run lengths in an online manner.

    Parameters
    ----------
    data : 2-demensional array
        data[t][j] : j-th feature of t-th observation, float
    hazard_func : function
        hazard_func(rs)[i] : hazard rate of run length rs[i], where rs is a numpy array
    observation_likelihood : instance of `Likelihood`
        predictor of time series

    Returns
    -------
    R : 2-dimensional ndarray
        R[r][t] : float
            the probability of the run length at time point `t` being `r`
    maxes : ndarray
        maxes[t] : MAP estimate of the run length as point `t`
    """

    maxes = np.zeros(len(data) + 1)

    R = np.zeros((len(data) + 1, len(data) + 1))
    R[0, 0] = 1

    for t, x in enumerate(data):
        # Evaluate the predictive distribution for the new datum under each of
        # the parameters.  This is the standard thing from Bayesian inference.
        predprobs = observation_likelihood.pdf(x)

        # Evaluate the hazard function for this interval
        H = hazard_func(np.array(range(t + 1)))

        # Evaluate the growth probabilities - shift the probabilities down and to
        # the right, scaled by the hazard function and the predictive
        # probabilities.
        R[1:t + 2, t + 1] = R[0:t + 1, t] * predprobs * (1 - H)

        # Evaluate the probability that there *was* a changepoint and we're
        # accumulating the mass back down at r = 0.
        R[0, t + 1] = np.sum(R[0:t + 1, t] * predprobs * H)

        # Renormalize the run length probabilities for improved numerical
        # stability.
        R[:, t + 1] = R[:, t + 1] / np.sum(R[:, t + 1])

        # Update the parameter sets for each possible run length.
        observation_likelihood.update_theta(x)

        maxes[t] = R[:, t].argmax()
    return R, maxes


def constant_hazard(lam, r):
    return 1 / lam * np.ones(r.shape)


class Predictor(metaclass=ABCMeta):

    @abstractmethod
    def pdf(self, data):
        """
        Probability density function

        Parameters
        ----------
        data : ndarray
            observations

        Returns
        -------
        pdfs : ndarray
            pdf evaluated at each point x in data
        """
        pass

    @abstractmethod
    def update_theta(self, x):
        """
        Update inner state.

        Parameters
        ----------
        x : _
            new observation
        """
        pass


class StudentT(Predictor):

    def __init__(self, alpha, beta, kappa, mu):
        self.alpha0 = self.alpha = np.array([alpha])
        self.beta0 = self.beta = np.array([beta])
        self.kappa0 = self.kappa = np.array([kappa])
        self.mu0 = self.mu = np.array([mu])

    def pdf(self, data):
        return stats.t.pdf(
            x=data, df=2 * self.alpha, loc=self.mu,
            scale=np.sqrt(self.beta * (self.kappa + 1) / (self.alpha * self.kappa))
        )

    def update_theta(self, data):
        muT0 = np.concatenate((self.mu0, (self.kappa * self.mu + data) / (self.kappa + 1)))
        kappaT0 = np.concatenate((self.kappa0, self.kappa + 1.))
        alphaT0 = np.concatenate((self.alpha0, self.alpha + 0.5))
        betaT0 = np.concatenate(
            (self.beta0, self.beta + (self.kappa * (data - self.mu)**2) / (2. * (self.kappa + 1.)))
        )

        self.mu = muT0
        self.kappa = kappaT0
        self.alpha = alphaT0
        self.beta = betaT0


def batch_estimate(data, hazard_func, observation_likelihood):
    """
    Compute MAP estimate of change points in a batch manner.

    Parameters
    ----------
    data : 2-demensional array
        data[t][j] : j-th feature of t-th observation, float
    hazard_func : function
        hazard_func(rs)[i] : hazard rate of run length rs[i], where rs is a numpy array
    observation_likelihood : instance of `Likelihood`
        predictor of time series

    Returns
    -------
    log_likelihood : float
        logarithmic likelihood of the MAP estimate
    change_points : list
        changepoints[j] : int, j-th change point
        array of change points, sorted in increasing order, starting with changepoints[0] = 0

    >>> from functools import partial
    >>> from scipy import stats
    >>> data = np.arange(0, 10, 0.1)# stats.norm.rvs(size=100)
    >>> h = partial(constant_hazard, 10)
    >>> lik = StudentT(1, 1, 1, 1)
    >>> batch_estimate(data, h, lik)
    (-13.044851791760607, [0, 11, 23, 40, 65, 100])
    """

    # Posteriors of run lengths
    R, _ = online_changepoint_detection(data, hazard_func, observation_likelihood)
    # Nexus
    # N[t] = (log_max_posterior_of_MAP(x_1^t), t, last_change_point_from_t)
    N = [(0.0, 0, None)]
    for t, colR in enumerate(R.T[1:], start=1):  # ignore the first column
        log_prob_r = np.log(colR[:t])
        ll_max, lcp_max = max((ll + lpr, t) for (ll, t, _), lpr in zip(reversed(N), log_prob_r))
        N.append((ll_max, t, lcp_max))
    # Restore changepoints
    invC = []
    t = len(N) - 1
    while t is not None:
        invC.append(t)
        _, _, t = N[t]
    return (N[-1][0], invC[::-1])
