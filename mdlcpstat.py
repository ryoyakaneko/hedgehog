######mdlcpstat.py
import numpy as np
import scipy.linalg as sl


class MDLCPStat:

    def __init__(self, h, lossfunc=None, fill_blank='none'):
        self.h = h
        self.lossfunc = loss_gaussian if lossfunc is None else lossfunc
        self.fill_blank = fill_blank
        assert fill_blank in ('none', 'online', 'offline')

    def transform(self, X):
        Z = delayNstack(X, 2 * self.h)
        score = list(map(_mdlcpstat(self.lossfunc), Z))
        if self.fill_blank == 'offline':
            score = [np.nan] * self.h + score + [np.nan] * self.h
        if self.fill_blank == 'online':
            score = [np.nan] * (self.h * 2) + score
        return np.array(score)


def delayNstack(X, depth): #####
    for i in range(len(X) - depth):#####
        yield X[i: i + depth]


def _mdlcpstat(lossfunc):
    loss = lossfunc

    def stat(X):
        if np.isnan(X).any():
            return np.nan
        n = len(X)#####
        return loss(X) - loss(X[:n // 2]) - loss(X[n // 2:])
    return stat


def loss_gaussian(X):
    Xmat = np.matrix(X)
    n,m = Xmat.shape
    if n == 1:
        Xmat = Xmat.T
        n,m = Xmat.shape
    else:
        pass
    if n <= 0:
        return np.nan
    Xc = Xmat - np.mean(Xmat,0)
    S = np.dot(Xc.T, Xc / n)
    detS = sl.det(S)
    logpdf_max = n * (-m - m * np.log(2 * np.pi) - np.log(detS)) / 2
    capacity = m * (m + 3) / 4 * np.log(n)  # approximation
    return -logpdf_max + capacity


def multigamma_ln(a,d):
    return special.multigammaln(a,d)

def lnml_gaussian(X):
    multigammaln = multigamma_ln
    n = len(X)
    Xmat = X.reshape((n, -1))
    n,m = Xmat.shape
    if n <= 0:
        return np.nan
    nu = m         ### given
    sigma = 1      ### given
    sum_x = np.sum(Xmat, axis = 0)
    sum_xxT = sum([xi.reshape((m, -1)) @ xi.reshape((-1, m)) for xi in Xmat])
    mu = sum_x / (n + nu)
    S = (sum_xxT + nu * sigma ** 2 * np.identity(m))/(n + nu) - mu.reshape((m, -1)) @ mu.reshape((-1, m))
    detS = sl.det(S)
    log_lnml =  m/2 * ((nu + 1) * np.log(nu) - n * np.log(np.pi) - (n+nu+1) * np.log(n + nu)) + multigammaln((n+nu)/2,m) - multigammaln(nu/2,m) + m * nu * np.log(sigma) - (n + nu)/2 * np.log(detS)
    return -1 * log_lnml




def loss_regression(X):
    Xmat = np.matrix(X)
    n = Xmat.shape[0]
    if n <= 0:
        return np.nan
    W = np.ones((2, n))
    for i in range(0,n):
        W[1,i] = i+1
    beta = sl.inv(W.dot(W.T)).dot(W).dot(X)
    Xc = Xmat - W.T.dot(beta)
    var = Xc.T * Xc/n
    logpdf_max = n * (-1 - np.log(2 * np.pi) - np.log(var)) / 2
    capacity = np.log(n)
    return -logpdf_max + capacity
