### mdlcpstat.py
import numpy as np
import scipy.linalg as sl
from scipy import special
from collections import deque
import functools as fts

class MDLCPStat_adwin:
    
    def __init__(self, lossfunc=None, how_to_drop = 'none'):
        self.lossfunc = loss_gaussian if lossfunc is None else lossfunc
        self.how_to_drop = how_to_drop
        assert how_to_drop in ('none', 'cutpoint', 'all')

    def transform(self, X, d, delta):

        # input: data(multi-dimension vector), epsilon
        # output: size(np.array)
        #         size[0] : window size
        #         size[1] : max(score)

        
        window = deque([])
        n = len(X)
        size = np.zeros((2, n)) # [0]: window size, [1]: score_max
        for t in range(0, n):
            window.append(X[t])
            if len(window) == 1:
                size[:, t] = 1, 0
                continue
            list_window = list(window)
            score = _mdlcpstat_adwin(self.lossfunc, list_window)
            i = len(score) - 1
            nn = len(list_window)
            threshold = np.log(1/delta) + (d/2 + 2 + delta) * np.log(nn)
            while score[i] <= threshold:
                i = i - 1
                if i < 0:
                    break
            cut = i
            if cut != -1:
                if self.how_to_drop == 'cutpoint':
                    cut = np.argmax(score)
                    for j in range(0, cut + 1):
                        window.popleft()
                    size[:, t] = len(window), max(score)
                if self.how_to_drop == 'all':
                    window = deque([])
                    size[:, t] = 0, max(score)                   
            else:
                size[:, t] = len(window), max(score)        
        return size


def _mdlcpstat_adwin(lossfunc, X):
    Xmat = np.matrix(X)
    n,m = Xmat.shape
    if n == 1:
        Xmat = Xmat.T
        n,m = Xmat.shape
    else:
        pass
    sum_x = np.zeros((n,m))
    sum_x[0] = Xmat[0]
    for i in range(0, n-1):
        sum_x[i+1] = sum_x[i] + Xmat[i+1]
    sum_xxT = np.zeros((n,m,m))
    sum_xxT[0] = Xmat[0].T.dot(Xmat[0])
    for i in range(0, n-1):
        sum_xxT[i+1] = sum_xxT[i] + Xmat[i+1].T.dot(Xmat[i+1])
    stat_list = np.zeros(n-1)  
    for cut in range(1,n):
        stat_list[cut - 1] = lossfunc(Xmat, sum_x[n - 1], sum_xxT[n - 1]) - lossfunc(Xmat[:cut], sum_x[cut - 1], sum_xxT[cut - 1]) - lossfunc(Xmat[cut:], sum_x[n - 1] - sum_x[cut - 1], sum_xxT[n - 1] - sum_xxT[cut - 1])
    return stat_list
#    return stat_list/np.log(n) ### divide by n


@fts.lru_cache(maxsize = None)
def multigamma_ln(a,d):
    return special.multigammaln(a,d)


def lnml_gaussian(X, sum_x, sum_xxT):
    multigammaln = multigamma_ln
    n,m = X.shape
    if n <= 0:
        return np.nan
    nu = m         ### given
    sigma = 1      ### given
    mu = sum_x / (n + nu)
    S = (sum_xxT + nu * sigma ** 2 * np.matrix(np.identity(m)))/(n + nu) - mu.T.dot(mu)
    detS = sl.det(S)
    log_lnml =  m/2 * ((nu + 1) * np.log(nu) - n * np.log(np.pi) - (n+nu+1) * np.log(n + nu)) + multigammaln((n+nu)/2,m) - multigammaln(nu/2,m) + m * nu * np.log(sigma) - (n + nu)/2 * np.log(detS)
    return -1 * log_lnml


##############################################################################




