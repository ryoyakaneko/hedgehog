import numpy as np
import mdlcpstat_adwin as mdladwn
import matplotlib.pyplot as plt
import subprocess as sub


# generate data (normal gaussian, 1 change point)
rng = np.random.RandomState(123)
data = rng.normal(size=1000)
for i in range(500,1000):
    data[i] = data[i] * 4

# calculate change scores
ins = mdladwn.MDLCPStat_adwin(lossfunc=mdladwn.lnml_gaussian)
size = ins.transform(X=data, d = 2, delta = 1)

#plot
f, a = plt.subplots(3, 1)
a[0].plot(data)
a[1].plot(size[0]) # window size
a[1].plot(size[1]) # MDL statistics
plt.show()

